<?php

namespace Modules\Supervisor\Repositories;

use App\Models\SupervisorRpostLaunchRecord;

/**
 * Класс-репозиторий для общения модуля мониторинга с БД
 *
 * @author Oleg Pyatin
 */
class SupervisorRepository
{
    /**
     * Функция сохранения новой записи о запуске скрипта
     *
     * @param SupervisorRpostLaunchRecord $new_supervisor_rpost_launch_record  Модель с начальными данными
     * @return int  ID новой записи в базе
     */
    public function saveLaunchRecord(SupervisorRpostLaunchRecord $new_supervisor_rpost_launch_record)
    {
        $new_supervisor_rpost_launch_record->save();
    }
}
