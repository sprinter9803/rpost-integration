<?php

namespace Modules\Supervisor\Services;

use Modules\Supervisor\Services\SupervisorLaunchService;
use Modules\Supervisor\Services\SupervisorOrderService;

/**
 * Входной сервис для запуска нужных действий логирования и мониторинга (имеет характер Фасада)
 *
 * @author Oleg Pyatin
 */
class SupervisorService
{
    protected $supervisor_launch_service;

    protected $supervisor_order_service;

    public function __construct(SupervisorLaunchService $supervisor_launch_service,
                                SupervisorOrderService $supervisor_order_service)
    {
        $this->supervisor_launch_service = $supervisor_launch_service;
        $this->supervisor_order_service = $supervisor_order_service;
    }

    public function recordStart()
    {
        $this->supervisor_launch_service->recordStart();
    }

    public function recordLoadBaseData()
    {
        $this->supervisor_launch_service->recordLoadBaseData();
    }
}
