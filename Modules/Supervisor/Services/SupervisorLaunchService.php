<?php

namespace Modules\Supervisor\Services;

use Modules\Supervisor\Repositories\SupervisorRepository;
use Modules\Supervisor\Factories\SupervisorFactory;
use Modules\Supervisor\Entities\SupervisorSession;
use Modules\Supervisor\Entities\SupervisorProcessValues;

/**
 * Сервис для выполнения работы мониторинга запуска и работы API оформления заказа
 *
 * @author Oleg Pyatin
 */
class SupervisorLaunchService
{
    protected $supervisor_repository;

    protected $supervisor_factory;

    protected $supervisor_session;

    public function __construct(SupervisorRepository $supervisor_repository, SupervisorFactory $supervisor_factory)
    {
        $this->supervisor_repository = $supervisor_repository;
        $this->supervisor_factory = $supervisor_factory;

        $this->supervisor_session = app()->make(SupervisorSession::class);
    }

    /**
     * Функция инициирования сохранения записи о запуске скрипта
     *
     * @return void  Выполняем сохранение полученной записи в условный объект сессии где хранятся переменные для мониторинга
     */
    public function recordStart()
    {
        $record = $this->supervisor_factory->createSupervisorRpostLaunchRecord();
        $this->supervisor_repository->saveLaunchRecord($record);

        $this->supervisor_session->setLaunchRecord($record);
    }

    /**
     * Функция установки статуса записи о работе скрипта к этапу перед загрузкой данных от системы хранения
     */
    public function recordLoadBaseData()
    {
        $this->supervisor_session->setLaunchRecordStatus(SupervisorProcessValues::SUPERVISOR_STATUS_LAUNCH_RPOST_LOAD_BASE_DATA);
        $this->supervisor_repository->saveLaunchRecord($this->supervisor_session->getLaunchRecord());
    }
}
