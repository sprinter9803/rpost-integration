<?php

namespace Modules\Supervisor\Services;

use Modules\Supervisor\Repositories\SupervisorRepository;
use Modules\Supervisor\Factories\SupervisorFactory;

use Modules\MyWarehouse\Entities\MwOrderData;

/**
 * Сервис для выполнения работы мониторинга и логирования действий над заказами
 *
 * @author Oleg Pyatin
 */
class SupervisorOrderService
{
    protected $supervisor_repository;

    protected $supervisor_factory;

    public function __construct(SupervisorRepository $supervisor_repository, SupervisorFactory $supervisor_factory)
    {
        $this->supervisor_repository = $supervisor_repository;
        $this->supervisor_factory = $supervisor_factory;
    }

    /**
     * Функция сохранения данных идущих от ERP
     *
     * @param MwOrderData $mw_order_data  Данные полученные от ERP-системы
     */
    public function saveBaseData(MwOrderData $mw_order_data)
    {

    }
}
