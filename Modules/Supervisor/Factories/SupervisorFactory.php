<?php

namespace Modules\Supervisor\Factories;

use App\Models\SupervisorRpostOrderRecord;
use App\Models\SupervisorRpostLaunchRecord;

use Modules\Supervisor\Entities\SupervisorProcessValues;

/**
 * Класс-фабрика для создания объектов модуля мониторинга
 *
 * @author Oleg Pyatin
 */
class SupervisorFactory
{
    /**
     * Функция для создания новой записи об обработке заказа для системы мониторинга
     *
     * @return SupervisorRpostOrderRecord
     */
    public function createSupervisorRpostOrderRecord(): SupervisorRpostOrderRecord
    {

    }

    /**
     * Функция для создания новой записи о запуске скрипта для системы мониторинга
     *
     * @return SupervisorRpostLaunchRecord
     */
    public function createSupervisorRpostLaunchRecord(): SupervisorRpostLaunchRecord
    {
        $new_supervisor_launch_record = new SupervisorRpostLaunchRecord();

        $new_supervisor_launch_record->status = SupervisorProcessValues::SUPERVISOR_STATUS_LAUNCH_RPOST_STARTING_WORK;
        $new_supervisor_launch_record->orders_id_set = '{}';

        return $new_supervisor_launch_record;
    }
}
