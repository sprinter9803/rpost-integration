<?php

namespace Modules\Supervisor\Entities;

/**
 * Класс для хранения вспомогательных значений работы с модулем мониторинга
 *
 * @author Oleg Pyatin
 */
class SupervisorProcessValues
{
    /**
     * Статус запуска сервиса - начало работы сервиса
     */
    public const SUPERVISOR_STATUS_LAUNCH_RPOST_STARTING_WORK = 'launch_starting_work';
    /**
     * Статус запуска сервиса - этап перед загрузкой данных от системы хранения заказов
     */
    public const SUPERVISOR_STATUS_LAUNCH_RPOST_LOAD_BASE_DATA = 'launch_load_base_data';
    /**
     * Статус запуска сервиса - этап перед валидацией данных
     */
    public const SUPERVISOR_STATUS_LAUNCH_RPOST_DATA_CHECKING = 'launch_data_checking';
    /**
     * Статус запуска сервиса - этап перед парсингом базовых параметров
     */
    public const SUPERVISOR_STATUS_LAUNCH_RPOST_BASE_PARAMS_PARSING = 'launch_base_params_parsing';
    /**
     * Статус запуска сервиса - этап перед парсингом адреса
     */
    public const SUPERVISOR_STATUS_LAUNCH_RPOST_ADDRESS_PARSING = 'launch_address_parsing';
    /**
     * Статус запуска сервиса - этап перед отправкой данных в Почту России (если это последний статус значит вывалилась скрипт обрушился)
     */
    public const SUPERVISOR_STATUS_LAUNCH_RPOST_DATA_SENDING = 'launch_data_sending';
    /**
     * Статус запуска сервиса - этап перед проверкой полученных от Почты России данных
     */
    public const SUPERVISOR_STATUS_LAUNCH_RPOST_SEND_RESULT_CHECKING = 'launch_send_result_checking';
    /**
     * Статус запуска сервиса - этап перед проверкой полученных от Почты России данных
     */
    public const SUPERVISOR_STATUS_LAUNCH_RPOST_ORDER_DATA_OBTAINING = 'launch_order_data_obtaining';
    /**
     * Статус запуска сервиса - этап перед проверкой полученных от Почты России данных
     */
    public const SUPERVISOR_STATUS_LAUNCH_RPOST_BATCH_MANAGEMENT = 'launch_batch_management';
    /**
     * Статус запуска сервиса - этап перед проверкой полученных от Почты России данных
     */
    public const SUPERVISOR_STATUS_LAUNCH_RPOST_FORMING_DOCUMENTS = 'launch_forming_documents';
    /**
     * Статус запуска сервиса - этап при отсылке результатов создания заказа обратно в систему хранения заказов
     */
    public const SUPERVISOR_STATUS_LAUNCH_RPOST_SENDING_RESPOND = 'launch_sending_respond';


    /**
     * Статус для записи заказа - сразу после загрузки из системы хранения
     */
    public const SUPERVISOR_STATUS_ORDER_RPOST_AFTER_LOAD = 'order_after_base_load';

}
