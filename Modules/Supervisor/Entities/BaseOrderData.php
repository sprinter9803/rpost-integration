<?php

namespace Modules\Supervisor\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для хранения приходящих от ERP-системы данных
 *
 * @author Oleg Pyatin
 */
class BaseOrderData
{
    /**
     * @var Данные о покупателе
     */
    public $agent_data;
    /**
     * @var Данные об организации
     */
    public $organization_data;
    /**
     * @var Данные адреса доставки
     */
    public $address_data;
    /**
     * @var Данные товаров в заказе
     */
    public $items_data;
    /**
     * @var Название заказа
     */
    public $order_name;
    /**
     * @var Сумма заказа
     */
    public $order_name;
}
