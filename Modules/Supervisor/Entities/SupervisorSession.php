<?php

namespace Modules\Supervisor\Entities;

use App\Models\SupervisorRpostLaunchRecord;

/**
 * Класс для хранения значений переменных во время работы скрипта для модуля мониторинга
 *     (Хранится в синглтоне Laravel)
 *
 * @author Oleg Pyatin
 */
class SupervisorSession
{
    /**
     * @var SupervisorRpostLaunchRecord Поле для хранения записи о запуске
     */
    private $launch_record;

    public function __construct()
    {
        $this->launch_record = null;
    }

    public function setLaunchRecord(SupervisorRpostLaunchRecord $supervisor_rpost_launch_record)
    {
        $this->launch_record = $supervisor_rpost_launch_record;
    }

    public function setLaunchRecordStatus(string $status)
    {
        $this->launch_record->status = $status;
    }

    public function getLaunchRecord()
    {
        return $this->launch_record;
    }
}
