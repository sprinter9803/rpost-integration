<?php

namespace Modules\Supervisor\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для хранения отправляемых в ТК систему данных
 *
 * @author Oleg Pyatin
 */
class SendingOrderData extends BaseDto
{

}
