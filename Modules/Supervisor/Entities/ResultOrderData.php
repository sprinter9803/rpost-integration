<?php

namespace Modules\Supervisor\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для хранения результирующих данных процесса (маркировка, себестоимость доставки, ошибки и пр)
 *
 * @author Oleg Pyatin
 */
class ResultOrderData extends BaseDto
{
    
}
