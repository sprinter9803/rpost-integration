<?php

namespace Modules\MyWarehouse\Components;

use Illuminate\Support\Facades\Http;

/**
 * Компонент для выполнения запросов к сервису МойСклад
 *
 * @author Oleg Pyatin
 */
class MyWarehouseConnector
{
    public function sendDataQuery(string $uri, array $data)
    {
        $response = Http::withHeaders([
            'Authorization'=>' Bearer '. getenv("MW_LOGIN_TOKEN")
        ])->withOptions([
            'verify'=>false
        ])->post($uri, $data);

        return $response->json();
    }

    public function sendSimpleQuery(string $uri)
    {
        $response = Http::withHeaders([
            'Authorization'=>' Bearer '. getenv("MW_LOGIN_TOKEN")
        ])->withOptions([
            'verify'=>false
        ])->get($uri);

        return $response->json();
    }

    public function updateDataQuery(string $uri, array $data)
    {
        $response = Http::withHeaders([
            'Authorization'=>' Bearer '. getenv("MW_LOGIN_TOKEN")
        ])->withOptions([
            'verify'=>false
        ])->put($uri, $data);

        return $response->json();
    }
}
