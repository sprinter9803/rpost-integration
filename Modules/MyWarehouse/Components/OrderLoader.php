<?php

namespace Modules\MyWarehouse\Components;

use Illuminate\Support\Facades\Storage;
use Modules\MyWarehouse\Entities\MwOrderData;

/**
 * Компонент для выполнения загрузки данных о заказе МойСклад из различных источников (от запроса,
 *     самой системы и др)
 *
 * @author Oleg Pyatin
 */
class OrderLoader
{
    /**
     * Функция загрузки данных для МойСклад
     */
    public function getMwOrderData()
    {
        $json_data = Storage::disk('public')->get('ms-order-example.json');
        $ms_json_order = json_decode($json_data, true);

        return MwOrderData::loadFromArray([
            'name'=>$ms_json_order["name"],
            'organization'=>$ms_json_order["organization"],
            'attributes'=>$ms_json_order["attributes"],
            'id'=>$ms_json_order["id"],
            'agent'=>$ms_json_order["agent"],
            'positions'=>$ms_json_order["positions"],
        ]);
    }

    public function parseOrganizationName(string $organization_name)
    {
        $msres = $v['organization'];
        $org_name = preg_replace( '/"([^"]*)"/', "«$1»", $msres['name'] );
    }
}
