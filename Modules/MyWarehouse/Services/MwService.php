<?php

namespace Modules\MyWarehouse\Services;

use Modules\MyWarehouse\Components\OrderLoader;
use Modules\MyWarehouse\Components\MyWarehouseConnector;
use Modules\MyWarehouse\Entities\MwProcessValues;
use Modules\MyWarehouse\Entities\MwOrderData;
use Modules\Supervisor\Services\SupervisorService;

/**
 * Сервис для выполнения действий связанных с системой МойСклад
 *
 * @author Oleg Pyatin
 */
class MwService
{
    protected $mw_loader;

    protected $supervisor;

    public function __construct(OrderLoader $mw_loader, MyWarehouseConnector $mw_connector,
                                SupervisorService $supervisor_service)
    {
        $this->mw_loader = $mw_loader;
        $this->mw_connector = $mw_connector;

        $this->supervisor = $supervisor_service;
    }

    public function getMwOrderData(string $mw_order_uuid)
    {
        $this->supervisor->recordLoadBaseData();

        $base_data = MwOrderData::loadFromArray($this->mw_connector->sendSimpleQuery(MwProcessValues::GET_ORDER_INFO_URL.
                $mw_order_uuid.MwProcessValues::EXTRA_ENTITY_INFO));

        return $this->prepareAttributesValues($base_data);
    }

    public function prepareAttributesValues(MwOrderData $base_data)
    {
        array_walk($base_data->attributes, function (&$value) {
            $value = [
                'id'=>$value["id"],
                'value'=>$value["value"]
            ];
        });

        $base_data->attributes = array_column($base_data->attributes, 'value', 'id');
        return $base_data;
    }

    /**
     * Функция получения всех заказов МС за последние 2 часа для Почты России и заполнение массива с ними
     *
     * @return array  Массив с DTO заказов
     */
    public function getPackageOrdersForTwoHours()
    {
        $orders_set = [];

        $time_limit_point = date("H:i:s", strtotime(date("H:i:s"). " -2 hour"));
        $certain_date = date("Y-m-d");

        $single_result = $this->mw_connector->sendSimpleQuery(MwProcessValues::MW_GET_ORDER_LIST_SIZE_URL_BASE_PART.
                $certain_date."%20".$time_limit_point.
                MwProcessValues::MW_GET_ORDER_LIST_URL_FILTER_NAME.
                MwProcessValues::MW_GET_ORDER_LIST_URL_FILTER_VALUE);


        $recent_orders_select_size = ceil($single_result["meta"]["size"] / 100);

//        for ($i=0; $i<$recent_orders_select_size; $i++) {
        for ($i=0; $i<MwProcessValues::MW_TEMPORARY_PACKAGE_SELECT_SIZE; $i++) {

            $offset_size = $i * 100;

            $package_result = $this->mw_connector->sendSimpleQuery(MwProcessValues::MW_GET_ORDER_LIST_NEXT_PACKAGE_URL_BASE_PART.
                $offset_size.'&updatedFrom='.$certain_date."%20".$time_limit_point.
                MwProcessValues::MW_GET_ORDER_LIST_URL_FILTER_NAME.
                MwProcessValues::MW_GET_ORDER_LIST_URL_FILTER_VALUE);

            sleep(6);

            for ($j=0; $j<count($package_result["rows"]); $j++) {

//                if ($package_result["rows"][$j]["state"]["name"]==="Доставлен") {
                if ($package_result["rows"][$j]["state"]["name"]===MwProcessValues::MW_TEMPORARY_PACKAGE_STATUS) {

                    $base_package_order_data = MwOrderData::loadFromArray($package_result["rows"][$j]);
                    $this->prepareAttributesValues($base_package_order_data);
                    $orders_set[] = $base_package_order_data;
                }
            }
        }

        return $orders_set;
    }


    /**
     * Функция получения тарифа для посылки - сначала пробуем взять из статичной таблицы, если тариф
     *     какой-то другой делаем запрос к МС
     *
     * @param string $tariff_name  Название тарифа
     * @param string $tariff_uri    URI для получения данных тарифа если не нашлось в справочнике
     * @return string  Код тарифа в системе СДЕК
     * @throws Exception  В случае если ничего не нашлось делаем исключение
     */
    public function getTariffCode(string $tariff_name, string $tariff_uri): string
    {
        // Сперва можно посмотреть по табличке - если не нашлось используем запрос
        if ($code = $this->getTariffCodeByName($tariff_name)) {
            return $code;
        }

        $tariff_code_from_mw = $this->getTariffByHref($tariff_uri)["code"] ?? false;

        if ($tariff_code_from_mw) {
            return $tariff_code_from_mw;
        } else {
            throw new Exception(MwProcessValues::ERROR_GET_TARIFF_DATA);
        }
    }

    public function getTariffByHref(string $tariff_uri)
    {
        return $this->mw_connector->sendSimpleQuery($tariff_uri);
    }

    public function prepareAssortmentAttrs(array $assortment_attributes)
    {
        array_walk($assortment_attributes, function (&$value) {
            $value = [
                'id'=>$value["id"],
                'value'=>$value["value"]
            ];
        });

        $prepared_assortment_attributes = array_column($assortment_attributes, 'value', 'id');
        return $prepared_assortment_attributes;
    }

    public function getCityInfoByHref(string $tariff_uri)
    {
        return $this->mw_connector->sendSimpleQuery($tariff_uri);
    }
}
