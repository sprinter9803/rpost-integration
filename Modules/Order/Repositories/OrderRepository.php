<?php

namespace Modules\Order\Repositories;

use App\Models\BatchDaySession;

/**
 * Класс-репозиторий для работы с БД в аспекте заказов
 *
 * @author Oleg Pyatin
 */
class OrderRepository
{
    /**
     * Функция сохранения новой записи о группе заказов
     *
     * @param  BatchDaySession $new_batch_day_session  Заполненная запись о создания новой группы заказов
     * @return  bool  Сохранилось или нет
     */
    public function saveNewBatchDaySession(BatchDaySession $new_batch_day_session)
    {
        return $new_batch_day_session->save();
    }

    /**
     * Функция получения имени сегодняшней партии заказов (если она есть)
     *
     * @return  ?string  Имя партии заказов или null если сегодня ничего не создавалось
     */
    public function getBatchDaySessionNameIfExist()
    {
        $day_session = BatchDaySession::where('batch_day', '=', date("Y-m-d"))->first();
        return $day_session->batch_name ?? null;
    }
}
