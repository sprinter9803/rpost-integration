<?php

namespace Modules\Order\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Routing\Controller;
use Modules\Order\Services\OrderService;
use Modules\MyWarehouse\Services\MwService;

class OrderController extends Controller
{
    protected $order_service;

    protected $mw_service;

    public function __construct(OrderService $order_service, MwService $mw_service)
    {
        $this->order_service = $order_service;
        $this->mw_service = $mw_service;
    }

    /**
     * Действие регистрации заказа в системе Почты России
     * @param $mw_order_uuid  UUID заказа в системе Мой Склад
     * @return Renderable
     */
    public function create(string $mw_order_uuid)
    {
        $mw_order_data = $this->mw_service->getMwOrderData($mw_order_uuid);
        return $this->order_service->createOrder($mw_order_data);
    }

    /**
     * Действие пакетной обработки заказов
     */
    public function createMultiple()
    {
        $mw_orders_set = $this->mw_service->getPackageOrdersForTwoHours();
        return $this->order_service->createOrdersPackage($mw_orders_set);
    }

    /**
     * Действие получения сведений о почтовом отделении по его индексу (для удобной проверки работоспособности API)
     * @param string $postal_place_code
     * @return type
     */
    public function findPostalPlace(string $postal_place_code)
    {
        return $this->order_service->getPostalPlaceInfo($postal_place_code);
    }

    public function deletePostalOrder(string $rp_delete_order_id)
    {
        return $this->order_service->deleteOrder($rp_delete_order_id);
    }

    public function getOrderInfo(string $rp_order_id)
    {
        return $this->order_service->getOrderInfo($rp_order_id);
    }

    /**
     * Действие получения сведений о заказах в указанной партии
     * @param string $batch_name  // Имя партии в системе Почты России
     * @return array  Сведения о заказах
     */
    public function getBatchOrders(string $batch_name)
    {
        return $this->order_service->getBatchOrders($batch_name);
    }
}
