<?php

namespace Modules\Order\Http\Middleware;

use Modules\Supervisor\Services\SupervisorService;
use Illuminate\Support\Arr;
use Closure;

/*
 * Middleware используемая для записи о действиях скрипта в модуль мониторинга
 *
 * @author Oleg Pyatin
 */
class RecordSupervisorRequestLaunch
{
    protected $supervisor;

    public function __construct(SupervisorService $supervisor_service)
    {
        $this->supervisor = $supervisor_service;
    }

    public function handle($request, Closure $next)
    {
        $this->supervisor->recordStart();
        return $next($request);
    }
}
