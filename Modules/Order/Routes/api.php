<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'order'], function () {
    Route::post('create/{mw_order_uuid}', 'OrderController@create')->middleware('supervisor.launch.record.start')->name('order.create');
    Route::delete('delete/{rp_delete_order_id}', 'OrderController@deletePostalOrder')->name('order.delete');
    Route::get('info/{rp_order_info}', 'OrderController@getOrderInfo')->name('order.info');

    Route::get('postal-place/{postal_place_code}', 'OrderController@findPostalPlace')->name('order.postal-place.find');

    Route::get('batch/{batch_name}', 'OrderController@getBatchOrders')->name('order.batch.orders');

    Route::post('create-multiple', 'OrderController@createMultiple')->name('order.create-multiple');
});
