<?php

namespace Modules\Order\Factories;

use App\Models\BatchDaySession;

/**
 * Класс-фабрика для создания записей в БД о выполняемых действиях и вспомогательных сущностях
 *
 * @author Oleg Pyatin
 */
class OrderFactory
{
    /**
     * Функция для создания новой записи о партии заказов
     * @param string $batch_name  ID у группы заказов в системе Почты России
     * @return BatchDaySession
     */
    public function createBatchDaySession(string $batch_name): BatchDaySession
    {
        $new_batch_day_session = new BatchDaySession();

        $new_batch_day_session->batch_name = $batch_name;
        $new_batch_day_session->batch_day = date("Y-m-d");

        return $new_batch_day_session;
    }
}
