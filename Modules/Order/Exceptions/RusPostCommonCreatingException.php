<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\RusPostOrderCreatingCustomException;

class RusPostCommonCreatingException extends RusPostOrderCreatingCustomException
{

}
