<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\RusPostOrderCreatingException;

class RusPostRecipientNotFullContactsException extends RusPostOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'Контакты пользователя заполнены не полностью';
    }
}
