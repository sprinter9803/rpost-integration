<?php

namespace Modules\Order\Exceptions;

use Modules\Order\Exceptions\Base\RusPostOrderCreatingException;

class RusPostDeliveryAddressEmptyException extends RusPostOrderCreatingException
{
    protected function specifiedTypeMessage()
    {
        return 'Адрес населенного пункта не заполнен';
    }
}
