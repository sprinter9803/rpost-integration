<?php

namespace Modules\Order\Exceptions\Base;

use Modules\MyWarehouse\Components\MyWarehouseConnector;
use Modules\MyWarehouse\Entities\MwProcessValues;
use Exception;

/**
 * Базовый класс исключений для ошибок с выводом внешней информации
 *
 * @author Oleg Pyatin
 */
class RusPostOrderCreatingCustomException extends Exception
{
    protected $mw_order_uuid;

    protected $mw_connector;

    public function __construct($message="", $code=0, Exception $previous=null, $mw_order_uuid)
    {
        $this->mw_order_uuid = $mw_order_uuid;
        $this->mw_connector = new MyWarehouseConnector();
        parent::__construct($message, $code, $previous);
    }

    public function report()
    {
        $this->mw_connector->updateDataQuery(MwProcessValues::MW_ORDER_CHANGE_URL.$this->mw_order_uuid, [
            'attributes'=>[
                [
                    'meta'=>[
                        'href'=> MwProcessValues::MW_ORDER_CHANGE_ATTR_URL . MwProcessValues::MW_ATTR_ERROR_MESSAGE,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>$this->message
                ],
            ],
        ]);
    }
}
