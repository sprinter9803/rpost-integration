<?php

namespace Modules\Order\Services;

use Modules\MyWarehouse\Entities\MwOrderData;
use Modules\RusPost\Entities\RusPostCreateData;
use Modules\RusPost\Entities\RusPostProcessValues;
use Modules\RusPost\Components\RusPostConnector;

/**
 * Сервис для выполнения распарсивания данных из заказа МС в заказ Почты России
 *
 * @author Oleg Pyatin
 */
class OrderTransferService
{
    protected $ruspost_connector;

    public function __construct(RusPostConnector $ruspost_connector)
    {
        $this->ruspost_connector = $ruspost_connector;
    }

    /**
     * Функция получения данных о весе заказа
     * @param  RusPostCreateData  DTO с данными для Почты России
     * @param  MwOrderData  DTO с данными для системы Мой Склад
     * @return  void  Заполняем массу в объект
     */
    public function getWeightData(RusPostCreateData &$rpost_create_data, MwOrderData $mw_order_data)
    {
        $full_weight = 0;

        foreach ($mw_order_data->positions["rows"] as $package) {

            $item_weight = 0;

            $item_quantity = $package["quantity"];

            if (isset($package["assortment"]["weight"])) {
                $item_weight = ($package["assortment"]["weight"]);
            }

            $full_weight += (int) (($item_quantity * $item_weight) * 1000);
        }

        if ($full_weight < 1) {
            $full_weight = 1;
        }

        $rpost_create_data->mass = $full_weight;
    }

    /**
     * Функция получения данных типа РПО
     * @param  RusPostCreateData  DTO с данными для Почты России
     * @param  MwOrderData  DTO с данными для системы Мой Склад
     * @return  void  Заполняем вид РПО в объект
     */
    public function getMailType(RusPostCreateData &$rpost_create_data, MwOrderData $mw_order_data)
    {
        // По-умолчанию тип посылки - Посылка "онлайн"
        $rpost_create_data->mail_type = RusPostProcessValues::MAIL_TYPE_ONLINE_PARCEL;

        if (isset($mw_order_data->attributes[RusPostProcessValues::MW_TARIFF_ATTR_UUID]["name"])) {

            switch ($mw_order_data->attributes[RusPostProcessValues::MW_TARIFF_ATTR_UUID]["name"]) {

                case RusPostProcessValues::ATTR_NAME_MAIL_TYPE_EMS:
                    $rpost_create_data->mail_type = RusPostProcessValues::MAIL_TYPE_EMS;
                    break;
                case RusPostProcessValues::ATTR_NAME_MAIL_TYPE_PARCEL_CLASS_ONE:
                    $rpost_create_data->mail_type = RusPostProcessValues::MAIL_TYPE_PARCEL_CLASS_ONE;
                    break;
                case RusPostProcessValues::ATTR_NAME_MAIL_TYPE_ONLINE_COURIER:
                    $rpost_create_data->mail_type = RusPostProcessValues::MAIL_TYPE_ONLINE_COURIER;
                    break;
            }
        }
    }

    /**
     * Функция проверки оплачен ли уже заказ (имеется ли наложенный платеж) и установки нужных параметров
     *     в этом случае
     * @param  RusPostCreateData  DTO с данными для Почты России
     * @param  MwOrderData  DTO с данными для системы Мой Склад
     * @return  void  Заполняем вид РПО в объект
     */
    public function processIfDeclaredValueCase(RusPostCreateData &$rpost_create_data, MwOrderData $mw_order_data)
    {
        if ($mw_order_data->attributes[RusPostProcessValues::MW_ATTR_ORDER_IS_PAYED] ?? false) {

            $rpost_create_data->mail_category = RusPostProcessValues::CATEGORY_WITH_DECLARED_VALUE_AND_CASH;
            $rpost_create_data->payment = $mw_order_data->sum;

        } else {

            $rpost_create_data->payment = 0;
        }
    }


    public function getAddressDataFromRusPost(RusPostCreateData &$rpost_create_data, MwOrderData $mw_order_data)
    {
        // Потребуется использование дополнительного запроса для нормализации адреса
        $mw_recipient_address = $mw_order_data->attributes[RusPostProcessValues::ATTR_RECIPIENT_MW_ORDER_ADDR];

        $rp_address_parsed = $this->ruspost_connector->sendPostQuery(RusPostProcessValues::RUSPOST_ADDRESS_CLEAN_URI, [
            [
                'id'=>'1',
                'original-address'=>$mw_recipient_address
            ]
        ]);

        $rpost_create_data->place_to = $rp_address_parsed[0]["place"] ?? ' ';
        $rpost_create_data->region_to = $rp_address_parsed[0]["region"] ?? ' ';
        $rpost_create_data->street_to = $rp_address_parsed[0]["street"] ?? ' ';
        $rpost_create_data->house_to = $rp_address_parsed[0]["house"] ?? ' ';
        $rpost_create_data->index_to = $rp_address_parsed[0]["index"] ?? ' ';

        $rpost_create_data->corpus_to = $mw_order_data->attributes[RusPostProcessValues::MW_ATTR_RECIP_ADDR_STRUCTURE] ?? ' ';
        $rpost_create_data->room_to = $mw_order_data->attributes[RusPostProcessValues::MW_ATTR_RECIP_ADDR_FLAT] ?? ' ';
    }
}
