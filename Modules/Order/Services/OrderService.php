<?php

namespace Modules\Order\Services;

use Modules\MyWarehouse\Entities\MwOrderData;
use Modules\RusPost\Entities\RusPostProcessValues;
use Modules\RusPost\Entities\RusPostCreateData;
use Modules\RusPost\Components\RusPostConnector;
use Modules\Order\Services\OrderTransferService;
use Modules\Order\Services\OrderReportService;
use Modules\Order\Services\OrderCheckService;
use Modules\Order\Entities\OrderProcessValues;

/**
 * Сервис для выполнения работы с заказами Почты России
 *
 * @author Oleg Pyatin
 */
class OrderService
{
    protected $ruspost_connector;

    protected $order_transfer_service;

    protected $order_report_service;

    public function __construct(RusPostConnector $ruspost_connector, OrderTransferService $order_transfer_service,
                                OrderReportService $order_report_service, OrderCheckService $order_check_service)
    {
        $this->ruspost_connector = $ruspost_connector;
        $this->order_transfer_service = $order_transfer_service;
        $this->order_report_service = $order_report_service;
        $this->order_check_service = $order_check_service;
    }

    public function createOrder(MwOrderData $mw_order_data)
    {
        $this->order_check_service->checkMwOrderData($mw_order_data);

        $ruspost_order_create_data = RusPostCreateData::loadFromArray([

            'address_type_to'=>RusPostProcessValues::DEFAULT_TYPE_ADDRESS,

            'mail_category'=>RusPostProcessValues::CATEGORY_WITH_DECLARED_VALUE,

            'mail_direct'=>RusPostProcessValues::RUSSIAN_FEDERATION_POST_CODE,

            'order_num'=>$mw_order_data->name,

            'recipient_name'=>$mw_order_data->agent["name"],

            'comment'=>$mw_order_data->description ?? $mw_order_data->attributes[RusPostProcessValues::MW_ATTR_COMMENT_UUID] ?? '',

            'fragile'=>RusPostProcessValues::DEFAULT_FRAGILE_TYPE,

            'tel_address'=>preg_replace('/[^0-9]/', '', $mw_order_data->agent["phone"]),

            'payment_method'=>RusPostProcessValues::PAYMENT_TYPE_CASHLESS,

            'sms-notice-recipient'=>RusPostProcessValues::DEFAULT_SMS_NOTICE_RECIPIENT,

            'insr_value'=>$mw_order_data->sum,

            'postoffice_code'=>RusPostProcessValues::POSTOFFICE_CODE_DEFAULT,
        ]);

        $this->order_transfer_service->getWeightData($ruspost_order_create_data, $mw_order_data);

        $this->order_transfer_service->getMailType($ruspost_order_create_data, $mw_order_data);

        $this->order_transfer_service->processIfDeclaredValueCase($ruspost_order_create_data, $mw_order_data);

        $this->order_transfer_service->getAddressDataFromRusPost($ruspost_order_create_data, $mw_order_data);




        $rp_order_result = $this->ruspost_connector->sendDataQuery(RusPostProcessValues::RUSPOST_ORDER_CREATE_URI, [$ruspost_order_create_data->getDataArrayInRusPostFormat()]);

        $this->order_check_service->checkRusPostAddOrderResults($rp_order_result, $mw_order_data->id);

        $new_rp_order_id = $rp_order_result["result-ids"][0];

        echo $new_rp_order_id;

        $this->order_report_service->doFinalActions($mw_order_data->id, $new_rp_order_id);

//        return $this->clearTestOrderAfterCreation($new_rp_order_id);
    }


    /**
     * Функция создания списка заказов
     *
     * @param array $mw_orders_set  Массив с DTO данных заказов
     * @return void  Создаем список заказов и ничего не возвращаем
     */
    public function createOrdersPackage(array $mw_orders_set)
    {
        for ($i=0; $i<count($mw_orders_set); $i++) {

            $ruspost_package_order_create_data = RusPostCreateData::loadFromArray([
                'address_type_to'=>RusPostProcessValues::DEFAULT_TYPE_ADDRESS,
                'mail_category'=>RusPostProcessValues::CATEGORY_WITH_DECLARED_VALUE,
                'mail_direct'=>RusPostProcessValues::RUSSIAN_FEDERATION_POST_CODE,
                'order_num'=>$mw_orders_set[$i]->name,
                'recipient_name'=>$mw_orders_set[$i]->agent["name"],
                'comment'=>$mw_orders_set[$i]->description ?? $mw_orders_set[$i]->attributes[RusPostProcessValues::MW_ATTR_COMMENT_UUID] ?? '',
                'fragile'=>RusPostProcessValues::DEFAULT_FRAGILE_TYPE,
                'tel_address'=>preg_replace('/[^0-9]/', '', $mw_orders_set[$i]->agent["phone"]),
                'payment_method'=>RusPostProcessValues::PAYMENT_TYPE_CASHLESS,
                'sms-notice-recipient'=>RusPostProcessValues::DEFAULT_SMS_NOTICE_RECIPIENT,
                'insr_value'=>$mw_orders_set[$i]->sum,
                'postoffice_code'=>RusPostProcessValues::POSTOFFICE_CODE_DEFAULT,
            ]);

            $this->order_transfer_service->getWeightData($ruspost_package_order_create_data, $mw_orders_set[$i]);

            $this->order_transfer_service->getMailType($ruspost_package_order_create_data, $mw_orders_set[$i]);

            $this->order_transfer_service->processIfDeclaredValueCase($ruspost_package_order_create_data, $mw_orders_set[$i]);

            $this->order_transfer_service->getAddressDataFromRusPost($ruspost_package_order_create_data, $mw_orders_set[$i]);


            $rp_package_order_result = $this->ruspost_connector->sendDataQuery(RusPostProcessValues::RUSPOST_ORDER_CREATE_URI, [$ruspost_package_order_create_data->getDataArrayInRusPostFormat()]);
            $new_rp_package_order_id = $rp_package_order_result["result-ids"][0];

            $this->order_report_service->doPackageFinalActions($mw_orders_set[$i]->id, $new_rp_package_order_id);
        }

        echo OrderProcessValues::RUSPOST_PACKAGE_SUCCESS_PROCESSING;
    }

    public function getOrderInfo(string $rp_order_id)
    {
        return $this->ruspost_connector->sendSimpleQuery(RusPostProcessValues::RUSPOST_ORDER_INFO_GET_URI . $rp_order_id);
    }

    public function deleteOrder(string $rp_delete_order_id)
    {
        return $this->ruspost_connector->deleteDataQuery(RusPostProcessValues::RUSPOST_ORDER_INFO_DELETE_URI, [$rp_delete_order_id]);
    }

    public function clearTestOrderAfterCreation(string $rp_test_order_id)
    {
        $this->deleteOrder($rp_test_order_id);

        if (isset($this->getOrderInfo($rp_test_order_id)["sub-code"]) &&
                $this->getOrderInfo($rp_test_order_id)["sub-code"] === RusPostProcessValues::RUSPOST_ORDER_NOT_EXIST_STATUS) {
            return PHP_EOL.RusPostProcessValues::DEVELOP_ORDER_DELETE_SUCCESSFULL;
        } else {
            return PHP_EOL.RusPostProcessValues::DEVELOP_ORDER_DELETE_ERROR;
        }
    }

    public function getPostalPlaceInfo(string $postal_place_code)
    {
        return $this->ruspost_connector->sendSimpleQuery(RusPostProcessValues::RUSPOST_POSTAL_PLACE_GET_URI . $postal_place_code);
    }

    /**
     * Функция получения данных о заказах в партии
     *
     * @param string $batch_name  Имя партии в системе Почты России
     * @return array  Результат запроса (данные о заказах в успешном случае)
     */
    public function getBatchOrders(string $batch_name)
    {
        return $this->ruspost_connector->sendSimpleQuery(RusPostProcessValues::RUSPOST_BATCH_INFO_URI_START . $batch_name .
                RusPostProcessValues::RUSPOST_BATCH_INFO_URI_END);
    }

    /**
     * Функция для получения нового тестового имени заказа (Для удобства отслеживания в ЛК-почты)
     */
    public function getTestingDevelopUniqueOrderNumber()
    {
        return "TEST-23-".mt_rand(1,100000);
    }
}
