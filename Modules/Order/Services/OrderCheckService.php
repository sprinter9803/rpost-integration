<?php

namespace Modules\Order\Services;

use Modules\Order\Exceptions\RusPostOrderTariffException;
use Modules\Order\Exceptions\RusPostCommonCreatingException;
use Modules\Order\Exceptions\RusPostRecipientNotFullContactsException;
use Modules\Order\Exceptions\RusPostDeliveryAddressEmptyException;
use Modules\MyWarehouse\Services\MwService;
use Modules\MyWarehouse\Entities\MwOrderData;
use Modules\RusPost\Entities\RusPostProcessValues;

/**
 * Сервис для организации логики проверки данных
 *
 * @author Oleg Pyatin
 */
class OrderCheckService
{
    protected $mw_service;

    public function __construct(MwService $mw_service)
    {
        $this->mw_service = $mw_service;
    }

    /**
     * Функция проверки корректности данных заказа МойСКлад
     *
     * @param MwOrderData $mw_order_data  DTO с данными заказа
     * @return void  Просто делаем проверку с выбросом исключения, если нужно
     */
    public function checkMwOrderData(MwOrderData $mw_order_data)
    {
        $this->checkRecipientContacts($mw_order_data);

        $this->checkDeliveryAddress($mw_order_data);
    }

    /**
     * Проверка корректного заполнения данных для пользователя (телефона)
     *
     * @param MwOrderData $mw_order_data  DTO с данными заказа
     */
    public function checkRecipientContacts(MwOrderData $mw_order_data)
    {
        if (empty($mw_order_data->agent["phone"])) {
            throw new RusPostRecipientNotFullContactsException("No info about phone", 0, null, $mw_order_data->id);
        }
    }

    /**
     * Проверка что заполнен адрес доставки
     *
     * @param MwOrderData $mw_order_data  DTO с данными заказа
     * @return void  Просто делаем проверку с выбросом исключения, если нужно
     */
    public function checkDeliveryAddress(MwOrderData $mw_order_data)
    {
        if (empty($mw_order_data->attributes[RusPostProcessValues::ATTR_RECIPIENT_MW_ORDER_ADDR])) {
            throw new RusPostDeliveryAddressEmptyException("No info about address", 0, null, $mw_order_data->id);
        }
    }

    /**
     * Функция проверки результатов добавления заказов
     *
     * @param array $rpost_add_order_results  Результат добавления заказа через API в форме массива
     * @return void  Просто выполняем действия и ничего не возвращаем
     */
    public function checkRusPostAddOrderResults(array $rpost_add_order_results, string $mw_order_uuid)
    {
        if (isset($rpost_add_order_results['code']) && ($rpost_add_order_results['code'] === OrderProcessValues::RUSPOST_TARIFF_ERROR)) {

            $tariff_error_details = (string)($rpost_add_order_results['details'] ?? '');
            throw new RusPostOrderTariffException("Возникли ошибки при создании заказа в Почте России, связанные с тарифами - ".$tariff_error_details, 0, null, $mw_order_uuid);
        }

        if (!empty($rpost_add_order_results['errors'])) {

            $error_text = $rpost_add_order_results['errors'][0]['error-codes'][0]['description'];
            throw new RusPostCommonCreatingException("Возникли ошибки при создании заказа в Почте России - ".$error_text, 0, null, $mw_order_uuid);
        }
    }
}
