<?php

namespace Modules\Order\Services;

use Modules\MyWarehouse\Entities\MwProcessValues;
use Modules\MyWarehouse\Components\MyWarehouseConnector;
use Modules\RusPost\Components\RusPostConnector;
use Modules\RusPost\Entities\RusPostProcessValues;
use Modules\Order\Entities\OrderDocumentData;
use Modules\Order\Entities\OrderProcessValues;
use Modules\Order\Factories\OrderFactory;
use Modules\Order\Repositories\OrderRepository;
use Illuminate\Support\Facades\Storage;

/**
 * Сервис для выполнения действий после успешного создания заказа в СДЕК
 *
 * @author Oleg Pyatin
 */
class OrderReportService
{
    protected $ruspost_connector;

    protected $mw_connector;

    protected $order_factory;

    protected $order_repository;

    public function __construct(RusPostConnector $ruspost_connector, MyWarehouseConnector $mw_connector,
                                OrderFactory $order_factory, OrderRepository $order_repository)
    {
        $this->ruspost_connector = $ruspost_connector;
        $this->mw_connector = $mw_connector;

        $this->order_factory = $order_factory;
        $this->order_repository = $order_repository;
    }

    /**
     * Выполнение результирующих действия после создания заказа
     *
     * @param string $base_mw_order_number  ID заказа в системе Мой Склад
     * @param string $new_rp_order_id  ID нового созданного заказа в Почте России
     * @return void  Просто выполняем действия и ничего не возвращаем
     */
    public function doFinalActions(string $base_mw_order_number, string $new_rp_order_id)
    {
        $rpost_order_data = $this->getOrderInfo($new_rp_order_id);

        $this->doBatchManageActions($new_rp_order_id);

        $order_document_data = $this->getDocumentData($new_rp_order_id, $rpost_order_data);

        $this->sendDocumentDataToMw($base_mw_order_number, $order_document_data);
    }

    /**
     * Выполнение действия помещения заказа в партию на сегодняшний день или создание новой партии,
     *     если таковой нету
     *
     * @param string $new_rp_order_id  ID нового заказа в системе Почты России
     * @return void  Выполняем действия с партиями и ничего не возвращаем
     */
    private function doBatchManageActions(string $new_rp_order_id)
    {
        $batch_day_session_name = $this->order_repository->getBatchDaySessionNameIfExist();

        if (empty($batch_day_session_name)) {

            $batch_data = $this->ruspost_connector->sendPostQuery(RusPostProcessValues::RUSPOST_BATCH_CREATE_URI_START . date("Y-m-d") . RusPostProcessValues::RUSPOST_BATCH_CREATE_URI_END, [
                $new_rp_order_id
            ]);

            $new_batch_id = $batch_data['batches'][0]['batch-name'] ?? false;

            if ($new_batch_id) {

                $new_batch_day_session = $this->order_factory->createBatchDaySession($new_batch_id);
                $this->order_repository->saveNewBatchDaySession($new_batch_day_session);
            }

        } else {

            $add_result = $this->ruspost_connector->sendPostQuery(RusPostProcessValues::RUSPOST_BATCH_ADD_POST_URI_START . $batch_day_session_name . RusPostProcessValues::RUSPOST_BATCH_ADD_POST_URI_END, [
                $new_rp_order_id
            ]);
        }
    }


    private function getDocumentData(string $new_rp_order_id, array $rpost_order_data)
    {
        return OrderDocumentData::loadFromArray([

            'barcode'=>$rpost_order_data['barcode'],

            'f7p_document'=>$this->formF7PDocument($new_rp_order_id),

            'delivery_sum'=> (int)$this->getDeliverySum($rpost_order_data)
        ]);
    }

    private function formF7PDocument(string $rpost_order_id)
    {
        $pdf = $this->ruspost_connector->processDataQuery(RusPostProcessValues::RUSPOST_F7P_CREATE_URI_START .
                                             $rpost_order_id . RusPostProcessValues::RUSPOST_F7P_CREATE_URI_END);

        Storage::disk(OrderProcessValues::SAVE_RUSPOST_FILES_DISK)->put($rpost_order_id.'.pdf', $pdf);

        return OrderProcessValues::BASE_F7P_DOMAIN_URI.$rpost_order_id.'.pdf';
    }

    private function getDeliverySum(array $rpost_order_array_data)
    {
        return ($rpost_order_array_data['ground-rate-with-vat'] + $rpost_order_array_data['insr-rate-with-vat']) / 100;
    }

    /**
     * Функция отправки результирующих данных (маркировки, себестоимости и пр) в МС
     *
     * @param OrderDocumentData $order_document_data  DTO-объект с хранимыми данными
     * @return void  Выполняем действие отправки и ничего не возвращаем
     */
    private function sendDocumentDataToMw(string $base_mw_order_number, OrderDocumentData $order_document_data)
    {
        $mw_additional_status_data = $this->mw_connector->sendSimpleQuery(MwProcessValues::MW_GET_ADDITIONAL_STATUS_URI);

        $result = $this->mw_connector->updateDataQuery(MwProcessValues::MW_ORDER_CHANGE_URL.$base_mw_order_number, [
            'attributes'=>[
                [
                    'meta'=>[
                        'href'=> MwProcessValues::MW_ORDER_CHANGE_ATTR_URL . MwProcessValues::MW_ATTR_ADDITIONAL_STATUS,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>[
                        'meta'=>$mw_additional_status_data["meta"],
                        'name'=>$mw_additional_status_data["name"],
                    ]
                ],

                [
                    'meta'=>[
                        'href'=> MwProcessValues::MW_ORDER_CHANGE_ATTR_URL . MwProcessValues::MW_ATTR_RUSPOST_BARCODE,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>$order_document_data->barcode
                ],
                [
                    'meta'=>[
                        'href'=> MwProcessValues::MW_ORDER_CHANGE_ATTR_URL . MwProcessValues::MW_ATTR_F7P_PRINT,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>$order_document_data->f7p_document
                ],
                [
                    'meta'=>[
                        'href'=> MwProcessValues::MW_ORDER_CHANGE_ATTR_URL . MwProcessValues::MW_ATTR_ERROR_MESSAGE,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>''
                ],
                [
                    'meta'=>[
                        'href'=> MwProcessValues::MW_ORDER_CHANGE_ATTR_URL . MwProcessValues::MW_ATTR_DELIVERY_COST,
                        'type'=>'attributemetadata',
                        'mediaType'=>'application/json',
                    ],
                    'value'=>$order_document_data->delivery_sum
                ],
            ],

            'state'=>[
                'meta'=>[
                    'href'=>MwProcessValues::MW_ORDER_CHANGE_STATE_URL . OrderProcessValues::MW_SUCCESS_RUSPOST_STATE,
                    'type'=>'state',
                    'mediaType'=>'application/json'
                ]
            ]
        ]);
    }

    /**
     * Выполнение результирующих действия для случая пакетной обработки
     *
     * @param string $base_mw_order_number  ID заказа в системе Мой Склад
     * @param string $new_rp_order_id  ID нового созданного заказа в Почте России
     * @return void  Просто выполняем действия и ничего не возвращаем
     */
    public function doPackageFinalActions(string $base_mw_order_number, string $new_rp_order_id)
    {
        $rpost_order_data = $this->getOrderInfo($new_rp_order_id);

        $this->doBatchManageActions($new_rp_order_id);
    }

    public function getOrderInfo(string $rp_order_id)
    {
        return $this->ruspost_connector->sendSimpleQuery(RusPostProcessValues::RUSPOST_ORDER_INFO_GET_URI . $rp_order_id);
    }
}
