<?php

namespace Modules\Order\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс для хранения результирующих данных о заказе (маркировка, себестоимость и пр)
 *
 * @author Oleg Pyatin
 */
class OrderDocumentData extends BaseDto
{
    /**
     * @var  Штрих-код
     */
    public $barcode;
    /**
     * @var  Себестоимость доставки
     */
    public $delivery_sum;
    /**
     * @var  Форма Ф7П
     */
    public $f7p_document;
}
