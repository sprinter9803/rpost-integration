<?php

namespace Modules\Order\Entities;

/**
 * Класс для хранения вспомогательной информации к работе с заказами
 *
 * @author Oleg Pyatin
 */
class OrderProcessValues
{
    /**
     * Обозначение Laravel-хранилища для сохраняемых от CDEK документов
     */
    public const SAVE_RUSPOST_FILES_DISK = 'pdf';
    /**
     * Обозначение домена для получения Ф7П-документов
     */
    public const BASE_F7P_DOMAIN_URI = 'https://i-colors.ru/post_rf/';

    /**
     * Указание статуса успешного состояния отправки для Почты России в Мой Склад
     */
    public const MW_SUCCESS_RUSPOST_STATE = '7ace2f7d-852d-11e8-9109-f8fc000c6adb';

    /**
     * Константа указывающая на ошибку с тарифами при добавлении заказа
     */
    public const RUSPOST_TARIFF_ERROR = 'TARIFF_ERROR';
    /**
     * Обозначение что пакетная обработка закончилась успешно
     */
    public const RUSPOST_PACKAGE_SUCCESS_PROCESSING = 'Пакетная обработка заказов закончилась успешно';
}

