<?php

namespace Modules\RusPost\Entities;

use App\Components\Dto\BaseDto;

/**
 * DTO-класс хранения данных для создания заказа в системе Почты России
 *
 * @author Oleg Pyatin
 */
class RusPostCreateData extends BaseDto
{
    /**
     * @var Тип адреса (4 варианта, обязательный параметр в API Почты)
     */
    public $address_type_to;

    /**
     * @var Номер здания (обязательный параметр в API Почты)
     */
    public $house_to;
    /**
     * @var Категория РПО (обязательный параметр в API Почты)
     */
    public $mail_category;
    /**
     * @var Код страны (обязательный параметр в API Почты)
     */
    public $mail_direct;
    /**
     * @var Вид РПО (обязательный параметр в API Почты)
     */
    public $mail_type;
    /**
     * @var Вес РПО (обязательный параметр в API Почты)
     */
    public $mass;


    /**
     * @var Линейные размеры
     */
    //public $dimension;

    /**
     * @var Товарное вложение РПО
     */
    //public $goods;

    /**
     * @var Номер заказа, внешний идентификатор заказа, который формируется отправителем (обязательный
     *          параметр в API Почты)
     */
    public $order_num;
    /**
     * @var Населенный пункт (обязательный параметр в API Почты)
     */
    public $place_to;
    /**
     * @var Наименование получателя (обязательный параметр в API Почты)
     */
    public $recipient_name;
    /**
     * @var Область, регион (обязательный параметр в API Почты)
     */
    public $region_to;
    /**
     * @var Часть адреса, улица (обязательный параметр в API Почты)
     */
    public $street_to;
    /**
     * @var Почтовый индекс (обязательный параметр в API Почты)
     */
    public $index_to;
    /**
     * @var Указание способа оплаты
     */
    public $payment_method;
    /**
     * @var Комментарий
     */
    public $comment;
    /**
     * @var Установлена ли отметка Осторожно/хрупкое (обязательный параметр в API Почты)
     */
    public $fragile;
    /**
     * @var Телефон получателя
     */
    public $tel_address;
    /**
     * @var Сумма наложенного платежа
     */
    public $payment;
    /**
     * @var Объявленная ценность
     */
    public $insr_value;
    /**
     * @var Часть здания: корпус
     */
    public $corpus_to;
    /**
     * @var Часть здания: номер помещения
     */
    public $room_to;
    /**
     * @var Индекс места приема
     */
    public $postoffice_code;

    /**
     * Функция для вывода данных в нужном Почте России формате (через простой дефис)
     * @return array  Данные подготовленные для вывода
     */
    public function getDataArrayInRusPostFormat()
    {
        return [
            'address-type-to'=>$this->address_type_to,
            'house-to'=>$this->house_to,
            'index-to'=>$this->index_to,
            'mail-category'=>$this->mail_category,
            'mail-direct'=>$this->mail_direct,
            'mail-type'=>$this->mail_type,
            'mass'=>$this->mass,
            'order-num'=>$this->order_num,
            'place-to'=>$this->place_to,
            'region-to'=>$this->region_to,
            'street-to'=>$this->street_to,
            'payment-method'=>$this->payment_method,
            'comment'=>$this->comment,
            'fragile'=>$this->fragile,
            'recipient-name'=>$this->recipient_name,
            'tel-address'=>$this->tel_address,
            'payment'=>$this->payment,
            'insr-value'=>$this->insr_value,
            'corpus-to'=>$this->corpus_to,
            'room-to'=>$this->room_to,
            'postoffice-code'=>$this->postoffice_code,  // Временно отключаем, но нужно настроить
        ];
    }
}
