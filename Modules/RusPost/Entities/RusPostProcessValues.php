<?php

namespace Modules\RusPost\Entities;

class RusPostProcessValues
{
    /**
     * URI для получения данных о почтовом отделении (в запросе добавляется индекс)
     */
    public const RUSPOST_POSTAL_PLACE_GET_URI = 'postoffice/1.0/';
    /**
     * URI для получения данных о заказе по его ID-ку
     */
    public const RUSPOST_ORDER_INFO_GET_URI = '1.0/backlog/';
    /**
     * URI для удаления заказа по его ID-ку
     */
    public const RUSPOST_ORDER_INFO_DELETE_URI = '1.0/backlog';
    /**
     * URI для создания заказа
     */
    public const RUSPOST_ORDER_CREATE_URI = '1.0/user/backlog';
    /**
     * URI для нормализации адреса
     */
    public const RUSPOST_ADDRESS_CLEAN_URI = '1.0/clean/address';
    /**
     * Первая часть URI для создание печатной формы Ф7П к заказу
     */
    public const RUSPOST_F7P_CREATE_URI_START = '1.0/forms/';
    /**
     * Вторая часть URI для создание печатной формы Ф7П к заказу
     */
    public const RUSPOST_F7P_CREATE_URI_END = '/f7pdf?print-type=THERMO';
    /**
     * Первая часть URI для создания партии из заказов (POST-запрос) (еще нужно добавить дату отсылки)
     */
    public const RUSPOST_BATCH_CREATE_URI_START = '1.0/user/shipment?sending-date=';
    /**
     * Вторая часть URI для создания партии из заказов (POST-запрос) (еще нужно добавить дату отсылки)
     */
    public const RUSPOST_BATCH_CREATE_URI_END = '&timezone-offset=0';
    /**
     * Первая часть URI для добавления заказа в группу (PUT-запрос)
     */
    public const RUSPOST_BATCH_ADD_POST_URI_START = '1.0/batch/';
    /**
     * Вторая часть URI для добавления заказа в группу (PUT-запрос)
     */
    public const RUSPOST_BATCH_ADD_POST_URI_END = '/shipment';
    /**
     * Первая часть URI для получения данных о заказах в партии
     */
    public const RUSPOST_BATCH_INFO_URI_START = '1.0/batch/';
    /**
     * Вторая часть URI для получения данных о заказах в партии
     */
    public const RUSPOST_BATCH_INFO_URI_END = '/shipment';



    /**
     * Внутреннее обозначение ситуации в системе Почты России когда заказа нету (используется в целях проверки удаления
     *     тестового заказа)
     */
    public const RUSPOST_ORDER_NOT_EXIST_STATUS = 'RESOURCE_NOT_FOUND';
    /**
     * Обозначение успешного результата при создании тестового заказа
     */
    public const DEVELOP_ORDER_DELETE_SUCCESSFULL = 'Заказ создан и успешно удален';
    /**
     * Обозначение ситуации когда тестовый заказ не был удален после создания
     */
    public const DEVELOP_ORDER_DELETE_ERROR = 'Тестовый заказ не был удален, нужно проверить список отправлений';



    /**
     * Указание кода РФ для системы Почты России
     */
    public const RUSSIAN_FEDERATION_POST_CODE = 643;
    /**
     * Указание категории РПО - С объявленной ценностью
     */
    public const CATEGORY_WITH_DECLARED_VALUE = 'WITH_DECLARED_VALUE';
    /**
     * Указание категории РПО - С объявленной ценностью и наложенным платежом
     */
    public const CATEGORY_WITH_DECLARED_VALUE_AND_CASH = 'WITH_DECLARED_VALUE_AND_CASH_ON_DELIVERY';
    /**
     * Указание вида РПО - "Посылка Онлайн"
     */
    public const MAIL_TYPE_ONLINE_PARCEL = 'ONLINE_PARCEL';
    /**
     * Указание вида РПО - "Отправление EMS"
     */
    public const MAIL_TYPE_EMS = 'EMS';
    /**
     * Указание вида РПО - "Посылка 1-го класса"
     */
    public const MAIL_TYPE_PARCEL_CLASS_ONE = 'PARCEL_CLASS_1';
    /**
     * Указание вида РПО - "Курьер онлайн"
     */
    public const MAIL_TYPE_ONLINE_COURIER = 'ONLINE_COURIER';
    /**
     * Внутреннее обозначение вида РПО в аттрибутах Почты России - "Посылка Онлайн"
     */
    public const ATTR_NAME_MAIL_TYPE_ONLINE_PARCEL = 'Почта РФ. Посылка онлайн';
    /**
     * Внутреннее обозначение вида РПО в аттрибутах Почты России - "Отправление EMS"
     */
    public const ATTR_NAME_MAIL_TYPE_EMS = 'Почта РФ. Отправление EMS';
    /**
     * Внутреннее обозначение вида РПО в аттрибутах Почты России - "Посылка 1-го класса"
     */
    public const ATTR_NAME_MAIL_TYPE_PARCEL_CLASS_ONE = 'Почта РФ. Посылка 1 класса';
    /**
     * Внутреннее обозначение вида РПО в аттрибутах Почты России - "Курьер онлайн"
     */
    public const ATTR_NAME_MAIL_TYPE_ONLINE_COURIER = 'Почта РФ. Курьер онлайн';
    /**
     * Указание способа оплаты - "Безналичный расчет"
     */
    public const PAYMENT_TYPE_CASHLESS = 'CASHLESS';
    /**
     * Указание типа адреса - Стандартный (улица, дом, квартира)
     */
    public const DEFAULT_TYPE_ADDRESS = 'DEFAULT';
    /**
     * Тип хрупкости посылки по-умолчаниб
     */
    public const DEFAULT_FRAGILE_TYPE = false;
    /**
     * Почтовый индекс места приема по-умолчанию
     */
    public const POSTOFFICE_CODE_DEFAULT = 102001;



    /**
     * UUID атрибута комментария
     */
    public const MW_ATTR_COMMENT_UUID = '9ffdbf2e-45ac-11e5-7a40-e89700218f1a';
    /**
     * UUID атрибута выбранного тарифа ТК
     */
    public const MW_TARIFF_ATTR_UUID = 'ebba2610-8a77-11e8-9109-f8fc0005e497';
    /**
     * UUID атрибута обозначения что заказ уже оплачен
     */
    public const MW_ATTR_ORDER_IS_PAYED = 'b3417c57-89dd-11e8-9109-f8fc0039ea47';
    /**
     * UUID атрибута адреса покупателя
     */
    public const ATTR_RECIPIENT_MW_ORDER_ADDR = '2a5b2325-49cf-11e6-7a69-97110010a6c2';
    /**
     * UUID атрибута строения адреса доставки
     */
    public const MW_ATTR_RECIP_ADDR_STRUCTURE = '2e4d754a-545e-11e9-912f-f3d400182b17';
    /**
     * UUID атрибута квартиры адреса доставки
     */
    public const MW_ATTR_RECIP_ADDR_FLAT = '6e31d481-545e-11e9-9107-504800182b49';


    /**
     * Указание признака SMS уведомления (правильно по-умолчанию)
     */
    public const DEFAULT_SMS_NOTICE_RECIPIENT = 1;

}
