<?php

namespace Modules\RusPost\Components;

use Illuminate\Support\Facades\Http;

class RusPostConnector
{
    public function sendDataQuery(string $uri, array $data)
    {
        $response = Http::withHeaders([
            'X-User-Authorization'=>' Basic '. getenv("RUSPOST_AUTH_KEY"),
            'Authorization'=>' AccessToken '. getenv("RUSPOST_AUTH_TOKEN"),
        ])->withOptions([
            'verify'=>false
        ])->put(getenv("RUSPOST_BASIC_URL").$uri, $data);

        return $response->json();
    }

    public function sendSimpleQuery(string $uri)
    {
        $response = Http::withHeaders([
            'X-User-Authorization'=>' Basic '. getenv("RUSPOST_AUTH_KEY"),
            'Authorization'=>' AccessToken '. getenv("RUSPOST_AUTH_TOKEN")
        ])->withOptions([
            'verify'=>false
        ])->get(getenv("RUSPOST_BASIC_URL").$uri);

        return $response->json();
    }

    public function deleteDataQuery(string $uri, array $data)
    {
        $response = Http::withHeaders([
            'X-User-Authorization'=>' Basic '. getenv("RUSPOST_AUTH_KEY"),
            'Authorization'=>' AccessToken '. getenv("RUSPOST_AUTH_TOKEN")
        ])->withOptions([
            'verify'=>false
        ])->delete(getenv("RUSPOST_BASIC_URL").$uri, $data);

        return $response->json();
    }

    public function sendPostQuery(string $uri, array $data)
    {
        $response = Http::withHeaders([
            'X-User-Authorization'=>' Basic '. getenv("RUSPOST_AUTH_KEY"),
            'Authorization'=>' AccessToken '. getenv("RUSPOST_AUTH_TOKEN"),
        ])->withOptions([
            'verify'=>false
        ])->post(getenv("RUSPOST_BASIC_URL").$uri, $data);

        return $response->json();
    }

    public function processDataQuery(string $uri)
    {
        $response = Http::withHeaders([
            'X-User-Authorization'=>' Basic '. getenv("RUSPOST_AUTH_KEY"),
            'Authorization'=>' AccessToken '. getenv("RUSPOST_AUTH_TOKEN")
        ])->withOptions([
            'verify'=>false
        ])->get(getenv("RUSPOST_BASIC_URL").$uri);

        return $response->getBody()->getContents();
    }
}
