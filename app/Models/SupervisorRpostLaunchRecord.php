<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SupervisorRpostLaunchRecord extends Model
{
    protected $table = 'supervisor_rpost_launch_record';

    use HasFactory;
}
