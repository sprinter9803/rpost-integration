<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BatchDaySession extends Model
{
    protected $table = 'batch_day_session';

    use HasFactory;
}
