<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SupervisorRpostOrderRecord extends Model
{
    protected $table = 'supervisor_rpost_order_record';

    use HasFactory;
}
