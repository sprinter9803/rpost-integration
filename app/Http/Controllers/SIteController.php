<?php

namespace App\Http\Controllers;

class SiteController extends Controller
{
    public function apiDocumentation()
    {
        return file_get_contents(storage_path('postman/RpostIntegration.postman_collection.json'));
    }
}
