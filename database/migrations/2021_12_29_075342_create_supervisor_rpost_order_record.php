<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupervisorRpostOrderRecord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supervisor_rpost_order_record', function (Blueprint $table) {
            $table->id();

            $table->string('base_order_id')->nullable;  // ID заказа в ERP-системе

            $table->text('rpost_base_data')->nullable;  // Данные полученные от ERP-системы

            $table->text('rpost_sending_data')->nullable;  // Данные отправляемые в систему ТК

            $table->text('rpost_result_data')->nullable;  // Результирующие данные (маркировка, себестоимость, возможные ошибки)

            $table->text('tc_api_response')->nullable;  // Прямой ответ от API ТК

            $table->string('status')->nullable;  // Статус каждого заказа в рамках сервиса отправки (включая отображение этапа ошибки)

            $table->text('error_text')->nullable;  // Первичный текст ошибки если таковые возникли

            $table->timestamps();
        });

        Schema::table('supervisor_rpost_order_record', function (Blueprint $table) {
            $table->index('base_order_id');
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supervisor_rpost_order_record', function (Blueprint $table) {
            $table->dropIndex('supervisor_rpost_order_record_base_order_id_index');
            $table->dropIndex('supervisor_rpost_order_record_status_index');
        });

        Schema::dropIfExists('supervisor_rpost_order_record');
    }
}
