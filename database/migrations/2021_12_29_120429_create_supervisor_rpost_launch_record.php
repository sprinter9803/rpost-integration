<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupervisorRpostLaunchRecord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supervisor_rpost_launch_record', function (Blueprint $table) {

            $table->id();

            $table->string('status')->nullable;  // Статус запуска скрипта

            $table->string('orders_id_set')->nullable;  // Массив ID обрабатываемых заказов

            $table->timestamps();
        });

        Schema::table('supervisor_rpost_launch_record', function (Blueprint $table) {
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supervisor_rpost_launch_record', function (Blueprint $table) {
            $table->dropIndex('supervisor_rpost_launch_record_status_index');
        });

        Schema::dropIfExists('supervisor_rpost_launch_record');
    }
}
