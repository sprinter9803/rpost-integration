<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBatchDaySession extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batch_day_session', function (Blueprint $table) {
            $table->id();
            $table->string('batch_name');
            $table->date('batch_day');
            $table->timestamps();
        });

        Schema::table('batch_day_session', function (Blueprint $table) {
            $table->index('batch_name');
            $table->index('batch_day');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('batch_day_session', function (Blueprint $table) {
            $table->dropIndex('batch_day_session_batch_name_index');
            $table->dropIndex('batch_day_session_batch_day_index');
        });

        Schema::dropIfExists('batch_day_session');
    }
}
